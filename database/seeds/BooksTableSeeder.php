<?php

use Illuminate\Database\Seeder;

class BooksTableSeede extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
            [
                [
                'created_at' => date('Y-m-d G:i:s'),
                'title' => 'The Great Gatsby',
                'author' => 'F. Scott Fitzgerald',
                'user_id' => 1,
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'title' => 'The Grapes of Wrath',
                'author' => 'John Steinbeck',
                'user_id' => 2,
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'title' => 'Nineteen Eighty-Four',
                'author' => 'George Orwell',
                'user_id' => 3,
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'title' => 'Ulysses',
                'author' => 'James Joyce',
                'user_id' => 4,
                ],
                [
                'created_at' => date('Y-m-d G:i:s'),
                'title' => 'Lolita',
                'author' => 'Vladimir Nabokov',
                'user_id' => 5,
                ],
            ]);
    }
}