<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'keren',
                    'email' => 'keren@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                    ],
                    [
                    'name' => 'reut',
                    'email' => 'reut@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Alon',
                    'email' => 'Alon@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'Orly',
                    'email' => 'Orly@gmail.com',
                    'password' =>Hash::make('123456789'),
                    'role' => 'Employees',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
            ]);
    }
}
