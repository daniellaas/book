@extends('layouts.app')
@section('content')

<h1> Add a new book </h1>
<form method = 'post' action="{{action('BookControllers@store')}}">
{{csrf_field()}}

<div class = "form-group">
<label for = "title" > What book would you like to add? </label>
<input type = "text" class ="form-control" name = "title">
</div>

<div class = "form-group">
<label for = "author" > Who is the author of the book? </label>
<input type = "text" class ="form-control" name = "author">
</div>

<div class ="form-group">
<input type= "submit" class = "form-control" name= "submit" value = "save">
</div>
</form>

@endsection