@extends('layouts.app')
@section('content')

<h1> This is your book list </h1>
<ul>
    @foreach($books as $book)
    <li>
    @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}" >
       @endif
   
    <a href = "{{route('books.edit',$book->id)}}" > title: {{$book->title}} author: {{$book->author}} </a>
    </li>
 
    @endforeach
</ul>
@can('manager')
<a href = "{{route('books.create')}}" >create new book</a>
@endcan
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
                   $.ajax({
                   url:"{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType: 'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script> 
@endsection


