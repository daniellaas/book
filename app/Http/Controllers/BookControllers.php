<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreBookPost;

class BookControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        if (Gate::denies('manager')) {
            $boss = DB::table('employees')->where('employee',$id)->first();
            $id = $boss->manager;
        }
        $user = User::find($id);
        $books = $user->books;
        return view('books.index', compact('books'));
        //$books = Book::all();
        //$books = User::find($id)->books;
        //return view('books.index',['books'=>$books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create books..");
        }
        return view ('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookPost $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }
        $validated = $request->validated();
        $books=new Book();
        $id = Auth::id();
        $books->title=$request->title;
        $books->author=$request->author;
        $books->status=0;
        $books->user_id=$id;
        $books->save();

        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }
        $book= Book::find($id);
        return view ('books.edit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        if (Gate::denies('manager')) {
            if ($request->has('update'))
                   abort(403,"You are not allowed to edit books..");
        } 
        if(Gate::allows('manager')){  
        if(!$book->user->id == Auth::id()) return(redirect('books'));
        $book->update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result' => 'success1','status' => $request->status ), 200);   
            } 
        }
        if(Gate::allows('employee')){
            if($book->status == 0){
            if(!$book->user->id == Auth::id()) return(redirect('books'));
              $book->update($request->except(['_token']));
                if($request->ajax()){
                     return Response::json(array('result' => 'success1','status' => $request->status ), 200);                      
                   }
              }
            }
         return redirect('books');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       } 
        $book = Book::find($id);
        //if($book->user->id == Auth::id()) return(redirect('books'));
        $book->delete();
        return redirect('books');
    }
}
